import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { ApprovedApplicationComponent } from './component/approved-application/approved-application.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { LoginComponent } from './component/login/login.component';
import { LogoutComponent } from './component/logout/logout.component';
import { LucComponent } from './component/luc/luc.component';
import { PendingforreviewComponent } from './component/pendingforreview/pendingforreview.component';
import { RejectedApplicationComponent } from './component/rejected-application/rejected-application.component';
import { ReturnedApplicationComponent } from './component/returned-application/returned-application.component';
import { ViewForApprovedComponent } from './component/view-for-approved/view-for-approved.component';
import { ViewForPendingComponent } from './component/view-for-pending/view-for-pending.component';
import { ViewForRejectedComponent } from './component/view-for-rejected/view-for-rejected.component';
import { ViewForReturnedComponent } from './component/view-for-returned/view-for-returned.component';
import { ViewimageComponentComponent } from './component/viewimage-component/viewimage-component.component';
import { AuthGuardService } from './service/auth-guard.service';
import { PendingfordisbursementComponent} from './component/pendingfordisbursement/pendingfordisbursement.component';
import { CompleteddisbursementComponent } from './component/completeddisbursement/completeddisbursement.component';
import{ ViewForPendingDisbursementComponent } from './component/view-for-pending-disbursement/view-for-pending-disbursement.component';
const routes: Routes = [
  { path: '', redirectTo: "login", pathMatch: "full" },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'viewImage', component: ViewimageComponentComponent, canActivate: [AuthGuardService] },
  {path: 'change_password',component:ChangePasswordComponent,canActivate:[AuthGuardService]},
  { path: 'pending_for_review', component: PendingforreviewComponent, canActivate: [AuthGuardService] },
  { path: 'rejected_applications', component: RejectedApplicationComponent, canActivate: [AuthGuardService] },
  { path: 'approved_applications', component: ApprovedApplicationComponent, canActivate: [AuthGuardService] },
  { path: 'returned_applications', component: ReturnedApplicationComponent, canActivate: [AuthGuardService] },
  { path: 'view_for_approved_details',component:ViewForApprovedComponent,canActivate:[AuthGuardService]},
  { path: 'view_for_reject_details', component: ViewForRejectedComponent, canActivate: [AuthGuardService] },
  { path: 'view_for_return_details',component:ViewForReturnedComponent,canActivate:[AuthGuardService]},
  { path: 'luc',component:LucComponent,canActivate:[AuthGuardService]},
  { path: 'pendingfordisbursement',component:PendingfordisbursementComponent,canActivate:[AuthGuardService]},
  { path: 'completeddisbursement',component:CompleteddisbursementComponent,canActivate:[AuthGuardService]},
  { path: 'view_for_pending_review', component: ViewForPendingComponent ,canActivate:[AuthGuardService]},
  { path: 'view_for_pending_disbursement', component: ViewForPendingDisbursementComponent ,canActivate:[AuthGuardService]},
  { path: 'logout', component: LogoutComponent,canActivate: [AuthGuardService] },
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
