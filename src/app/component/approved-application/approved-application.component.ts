import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApprovedModel } from 'src/app/model/approved-model';
import { ApprovedApplicationService } from 'src/app/service/approved-application.service';

@Component({
  selector: 'app-approved-application',
  templateUrl: './approved-application.component.html',
  styleUrls: ['./approved-application.component.css']
})
export class ApprovedApplicationComponent implements OnInit {

 
  approvedModel : ApprovedModel
  approvedApplications: any = []
  public pageSize: number = 5;
  searchText;
  p: number = 1;
  id:any = sessionStorage.getItem('username')

  constructor(private approvedApplicationService:ApprovedApplicationService, private router :Router) { }
  ngOnInit(): void {
   this.getData();
   this.approvedModel = new ApprovedModel();
  }

  getData() {
    this.approvedApplicationService.getData(this.id)
    .subscribe((data) => {
      this.approvedApplications.push(data)
      this.approvedApplications = this.approvedApplications[0]
     
    })
    
  }
  fetchData(event) {
    console.log("data has been recieved")
    this.approvedModel.approveCustomerData = []
    var selected_id = event.currentTarget.id
    this.approvedApplications.forEach(data => {
      if (selected_id == data.applicationid) {
        this.approvedModel.approveCustomerData.push(data)
        this.approvedApplicationService.approvedCustomerDatas(this.approvedModel.approveCustomerData)
        this.router.navigateByUrl('/view_for_approved_details')
      }
    })

  }
}