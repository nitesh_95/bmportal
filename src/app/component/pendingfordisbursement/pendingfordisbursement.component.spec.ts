import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingfordisbursementComponent } from './pendingfordisbursement.component';

describe('PendingfordisbursementComponent', () => {
  let component: PendingfordisbursementComponent;
  let fixture: ComponentFixture<PendingfordisbursementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingfordisbursementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingfordisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
