import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pendingfordisbursmentmodel } from 'src/app/model/pendingfordisbursmentmodel';
import { PendingfordisbursementService } from 'src/app/service/pendingfordisbursement.service';

@Component({
  selector: 'app-pendingfordisbursement',
  templateUrl: './pendingfordisbursement.component.html',
  styleUrls: ['./pendingfordisbursement.component.css']
})
export class PendingfordisbursementComponent implements OnInit {

  pendingfordisbursementmodel: Pendingfordisbursmentmodel
  pendingDisbursement: any = []
  public pageSize: number = 5;
  searchText;
  p: number = 1;
  id: any = sessionStorage.getItem('username')

  constructor(private pendingfordisbursementservice: PendingfordisbursementService, private router: Router) { }
  ngOnInit(): void {
    this.getData();
    this.pendingfordisbursementmodel = new Pendingfordisbursmentmodel();
  }

  getData() {
    this.pendingfordisbursementservice.getData()
      .subscribe((data) => {
        console.log(data)
        this.pendingDisbursement.push(data)
        this.pendingDisbursement = this.pendingDisbursement[0].disbursmentDetails

      })

  }
  fetchData(event) {
    this.pendingfordisbursementmodel.pendingDisbursement = []
    var selected_id = event.currentTarget.id
    this.pendingDisbursement.forEach(data => {
      console.log(selected_id)
      console.log(data)
      if (selected_id == data.applicationId) {
        this.pendingfordisbursementmodel.pendingDisbursement.push(data)
        this.pendingfordisbursementservice.pendingDisbursements(this.pendingfordisbursementmodel.pendingDisbursement)
        this.router.navigateByUrl('/view_for_pending_disbursement')
      }
    })

  }
}
