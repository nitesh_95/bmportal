import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChangePasswordModel } from 'src/app/model/change-password-model';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ChangePasswordService } from 'src/app/service/change-password.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  changePassword: ChangePasswordModel
  jsonData: any;
  constructor(
    private router: Router,
    private http: HttpClient,
    private authpass: AuthPassService, private changepasswordservice: ChangePasswordService) {
  }
  currentPassword: any
  common_IP: any;
  id: any
  ngOnInit() {
    this.changePassword = new ChangePasswordModel();

  }

  checkPassword() {
    this.changePassword.oldpassword = this.changePassword.oldpassword;
    this.changePassword.newpassword = this.changePassword.newpassword;
    this.changePassword.confirmpassword = this.changePassword.confirmpassword;
    this.changepasswordservice.checkPassword(this.changePassword.oldpassword, this.changePassword.newpassword, this.changePassword.confirmpassword)
      .subscribe(data => {
        if (data['status'] == '01') {
          alert("Password not changed.Something went wrong please Try Again");

        } else if (data['status'] == '00') {
          alert("Password changed successfully. Please Login Again");
          window.location.href = "/login"
        } else {
          alert("Something went wrong Please try again")
          window.location.reload();
        }
      })
  }
}
