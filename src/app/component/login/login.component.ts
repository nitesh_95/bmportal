import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/model/login-model';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {

 
login:LoginModel


  constructor(private loginService: LoginService,
    private router: Router, private loginmodel: LoginModel) { }

  ngOnInit() {
    this.login = new LoginModel();
  }

  checkLogin() {
  this.router.navigateByUrl('/dashboard')
  }
  // checkLogin() {
  //   this.loginService.checklogin(this.login.username, this.login.password)
  //     .subscribe(
  //       data => {
  //         if (data['status'] == '00' && data['portaltype'] == 'new') {
  //           this.router.navigate(['approved_applications']).then(() => {
  //             sessionStorage.setItem('branch', data['branchname'])
  //             window.location.reload();
  //           });
  //         } else if (data['status'] == '00' && data['portaltype'] == 'old') {
  //           sessionStorage.setItem('branch', data['branchname'])
  //           window.location.href = 'https://172.16.145.182:443/UBILoanOrigination/login?password=' + this.login.password + '&solId=' + this.login.username
  //         } else {
  //           alert('please Enter valid credentials')
  //         }
  //       })
  // }


}