import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteddisbursementComponent } from './completeddisbursement.component';

describe('CompleteddisbursementComponent', () => {
  let component: CompleteddisbursementComponent;
  let fixture: ComponentFixture<CompleteddisbursementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompleteddisbursementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteddisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
