import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Completeddisbursementmodel } from 'src/app/model/completeddisbursementmodel';
import { CompleteddisbursementService } from 'src/app/service/completeddisbursement.service';

@Component({
  selector: 'app-completeddisbursement',
  templateUrl: './completeddisbursement.component.html',
  styleUrls: ['./completeddisbursement.component.css']
})
export class CompleteddisbursementComponent implements OnInit {

  completeddisbursementmodel: Completeddisbursementmodel
  completedDisbursement: any = []
  public pageSize: number = 5;
  searchText;
  p: number = 1;
  id: any = sessionStorage.getItem('username')

  constructor(private completeddisbursmentservice: CompleteddisbursementService, private router: Router) { }
  ngOnInit(): void {
    this.getData();
    this.completeddisbursementmodel = new Completeddisbursementmodel();
  }

  getData() {
    this.completeddisbursmentservice.getData()
      .subscribe((data) => {
        console.log(data)
        this.completedDisbursement.push(data)
        this.completedDisbursement = this.completedDisbursement[0].disbursmentDetails

      })

  }

fetchData(event) {
    this.completeddisbursementmodel.completeddisbursmentData = []
    var selected_id = event.currentTarget.id
    this.completedDisbursement.forEach(data => {
      if (selected_id == data.applicationid) {
        this.completeddisbursementmodel.completeddisbursmentData.push(data)
        this.completeddisbursmentservice.completedDisbursements(this.completeddisbursementmodel.completeddisbursmentData)
        this.router.navigateByUrl('/view_for_approved_details')
      }
    })

}
}