import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewimageComponentComponent } from './viewimage-component.component';

describe('ViewimageComponentComponent', () => {
  let component: ViewimageComponentComponent;
  let fixture: ComponentFixture<ViewimageComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewimageComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewimageComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
