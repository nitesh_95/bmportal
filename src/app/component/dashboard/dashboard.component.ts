import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { HttpClient } from '@angular/common/http'
import { AuthPassService } from 'src/app/service/auth-pass.service'

import { Chart } from 'chart.js';
import { Router } from '@angular/router';

import { DatePipe } from '@angular/common';
import { DashboardService } from 'src/app/service/dashboard.service';
import { Dashboard } from 'src/app/model/dashboard';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  dashboardModel: Dashboard

  alwaysShowCalendars: boolean;
  ranges: any = {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    'Last Six Months': [moment().subtract(6, 'month'), moment().subtract(0, 'month').endOf('month')]
  }
  locale: any = {
    format: 'DD/MM/YYYY',
    displayFormat: 'DD/MM/YYYY',
    separator: ' To ',
    applyLabel: 'Okay'
  }
  minDate: moment.Moment = moment().subtract(1, 'year');
  maxDate: moment.Moment = moment().add(0, 'days');
  selected = { start: moment().subtract(3, 'month'), end: moment().add(0, 'days') };
  invalidDates: moment.Moment[] = [moment().add(2, 'days'), moment().add(3, 'days'), moment().add(5, 'days')];



  constructor(private authpass: AuthPassService,
    private dashboard: AuthPassService,
    private router: Router,
    private http: HttpClient,
    private datepipe: DatePipe, private dashboardSevice: DashboardService) {
    this.alwaysShowCalendars = true;

  }
  ngOnInit() {
    this.dashboardModel = new Dashboard();
    this.dashboardModel.id = sessionStorage.getItem('username');
    if (this.dashboardModel.barchart) {
      this.dashboardModel.barchart.destroy();
    }
    if (this.dashboardModel.piechart) {
      this.dashboardModel.piechart.destroy();
    }
    this.dashboardModel.model = '';
    this.dashboardModel.date = ''
    this.dashboardModel.common_IP = sessionStorage.getItem('commonIP');
    this.getChartCount(event)
    let t = this;

  }
  rangeClicked(range) {
    console.log('[rangeClicked] range is : ', range);
  }
  datesUpdated(range) {
    console.log('[datesUpdated] range is : ', range);
  }
  isInvalidDate = (m: moment.Moment) => {
    return this.invalidDates.some(d => d.isSame(m, 'day'))
  }
  chartcount() {
    var barchart
    var piechart
    this.dashboardModel.chartCountList.forEach(data => {
      this.dashboardModel.approvedCount = data.approvedCount;
      this.dashboardModel.pendingCount = data.pendingCount;
      this.dashboardModel.rejectedCount = data.rejectedCount;
      this.dashboardModel.returnedCount = data.returnedCount;

    });
    if (this.dashboardModel.barchart) {
      this.dashboardModel.barchart.destroy();
    }
    var canvas = this.dashboardModel.barchart
    var ctx = canvas.getContext('2d')
    this.dashboardModel.barchart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ["Pending(" + this.dashboardModel.pendingCount + ")", "Approved(" + this.dashboardModel.approvedCount + ")", "Return(" + this.dashboardModel.returnedCount + ")", "Reject(" + this.dashboardModel.rejectedCount + ")"],
        datasets: [{
          data: [this.dashboardModel
            .pendingCount, this.dashboardModel.approvedCount, this.dashboardModel
            .returnedCount, this.dashboardModel.rejectedCount],
          backgroundColor: [
            '#4f81bc',
            '#9bbb58',
            '#23bfaa',
            '#c0504e',
          ],
          borderColor: [
            '#4f81bc',
            '#9bbb58',
            '#23bfaa',
            '#c0504e'
          ],
          borderWidth: 1
        }]
      },
      options: {
        title: {
          text: "Bar Chart",
          display: true
        },
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        onClick: function (e) {
          var element = this.getElementAtEvent(e);
          var element_label = element[0]._view.label
          var element_label_split = element_label.split("(")
          if (element_label_split[0] == 'Approved') {
            location.href = '/bmportal/#/approved_applications';
          }
          if (element_label_split[0] == 'Pending') {
            location.href = '/bmportal/#/pending_for_review';
          }
          if (element_label_split[0] == 'Return') {
            location.href = '/bmportal/#/returned_applications';
          }
          if (element_label_split[0] == 'Reject') {
            location.href = '/bmportal/#/rejected_applications';
          }
        }
      }
    });
    if (this.dashboardModel.piechart) {
      this.dashboardModel.piechart.destroy();
    }
    var piecanvas = this.dashboardModel.piechart
    var piectx = piecanvas.getContext('2d')
    this.dashboardModel.piechart = new Chart(piectx, {
      type: 'pie',
      data: {
        labels: ["Pending(" + this.dashboardModel.pendingCount + ")", "Approved(" + this.dashboardModel.approvedCount + ")", "Return(" + this.dashboardModel.returnedCount + ")", "Reject(" + this.dashboardModel.rejectedCount + ")"],
        datasets: [{

          data: [this.dashboardModel.pendingCount, this.dashboardModel.approvedCount, this.dashboardModel.returnedCount, this.dashboardModel.rejectedCount],
          backgroundColor: [
            '#4f81bc',
            '#9bbb58',
            '#23bfaa',
            '#c0504e',
          ],
          borderColor: [
            '#4f81bc',
            '#9bbb58',
            '#23bfaa',
            '#c0504e',
          ],
          borderWidth: 1
        }],

      },
      options: {
        title: {
          text: "Pie Chart",
          display: true
        },
        onClick: function (e) {
          var element = this.getElementAtEvent(e);
          var element_label = element[0]._view.label
          var element_label_split = element_label.split("(")
          if (element_label_split[0] == 'Approved') {
            location.href = '/bmportal/#/approved_applications';
          }
          if (element_label_split[0] == 'Pending') {
            location.href = '/bmportal/#/pending_for_review';
          }
          if (element_label_split[0] == 'Return') {
            location.href = '/bmportal/#/returned_applications';
          }
          if (element_label_split[0] == 'Reject') {
            location.href = '/bmportal/#/rejected_applications';
          }
        }
      }
    });
  }

  getChartCount(event) {
    this.dashboardModel.chartCountList = []
    var startDate = this.selected.start['_d']
    var endDate = this.selected.end['_d']
    var startDateAfterTransform = this.datepipe.transform(startDate, 'd/M/yyyy')
    var endDateAfterTransform = this.datepipe.transform(endDate, 'd/M/yyyy')
    this.dashboardSevice.getChartCount(startDateAfterTransform, endDateAfterTransform)
      .subscribe(data => {

        this.dashboardModel.chartCountList.push(data)

        this.chartcount()
      })

  }
}