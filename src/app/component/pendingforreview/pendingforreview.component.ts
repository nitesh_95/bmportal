import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PendingModel } from 'src/app/model/pending-model';
import { PendingforReviewService } from 'src/app/service/pendingfor-review.service';

@Component({
  selector: 'app-pendingforreview',
  templateUrl: './pendingforreview.component.html',
  styleUrls: ['./pendingforreview.component.css']
})
export class PendingforreviewComponent implements OnInit {
  pendingForReview = []
  public pageSize: number = 5;
  searchText;
  p: number = 1;
  id:any = sessionStorage.getItem('username')
  
  pendingModel : PendingModel
  constructor(private pendingforreviewservice:PendingforReviewService, private router :Router) { }
  ngOnInit(): void {
   this.getData();
   this.pendingModel  = new PendingModel();
  }

  getData() {
    this.pendingforreviewservice.getData(this.id)
    .subscribe((data) => {
      this.pendingForReview.push(data)
      this.pendingForReview = this.pendingForReview[0]
     
    })
    
  }
  fetchData(event) {
    this.pendingModel.pendingCustomerData = []
    var selected_id = event.currentTarget.id
    this.pendingForReview.forEach(data => {
      if (selected_id == data.applicationid) {
        this.pendingModel.pendingCustomerData.push(data)
        this.pendingforreviewservice.pendingViewdata(this.pendingModel.pendingCustomerData)
        this.router.navigateByUrl('/view_for_pending_review')
      }
    })

  }
}