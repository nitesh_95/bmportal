import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RejectedApplicationService } from 'src/app/service/rejected-application.service'
@Component({
  selector: 'app-rejected-application',
  templateUrl: './rejected-application.component.html',
  styleUrls: ['./rejected-application.component.css']
})
export class RejectedApplicationComponent implements OnInit {
  rejectedApplicationData: any = []
  public pageSize: number = 5;
  common_IP: any;
  searchText;
  p: number = 1;
  id: any = sessionStorage.getItem('username');

  constructor(private rejectedApplicationService: RejectedApplicationService, private router: Router) { }
  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.rejectedApplicationService.getData(this.id)
      .subscribe((data) => {
        this.rejectedApplicationData.push(data)
        this.rejectedApplicationData = this.rejectedApplicationData[0]

      })

  }
  fetchData(event) {
    this.rejectedApplicationData = []
    var selected_id = event.currentTarget.id
    this.rejectedApplicationData.forEach(data => {
      if (selected_id == data.applicationid) {
        this.rejectedApplicationData.push(data)
        this.rejectedApplicationService.rejectCustomerDatas(this.rejectedApplicationData);
        this.router.navigateByUrl('/rejected_applications')
      }
    })

  }
}