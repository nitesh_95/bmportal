import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForRejectedComponent } from './view-for-rejected.component';

describe('ViewForRejectedComponent', () => {
  let component: ViewForRejectedComponent;
  let fixture: ComponentFixture<ViewForRejectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewForRejectedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
