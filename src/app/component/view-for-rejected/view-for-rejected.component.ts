import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
import { ViewForRejectedService } from 'src/app/service/view-for-rejected.service';
import { ViewForRejectedModel } from 'src/app/model/view-for-rejected-model';

@Component({
  selector: 'app-view-for-rejected',
  templateUrl: './view-for-rejected.component.html',
  styleUrls: ['./view-for-rejected.component.css']
})
export class ViewForRejectedComponent implements OnInit {

  viewforrejected: ViewForRejectedModel;

  constructor(
    private http: HttpClient,
    private viewApplication: ViewApplicationService,
    private router: Router, private ViewForRejectedService: ViewForRejectedService) { }
  rejectCustomerData: any = [];

  ngOnInit() {
    this.viewforrejected = new ViewForRejectedModel();
    this.viewforrejected.id = sessionStorage.getItem('username')
    this.rejectCustomerData = JSON.parse(sessionStorage.getItem('rejectCustomerData'))
    this.viewforrejected.common_IP = sessionStorage.getItem('commonIP')
    this.rejectCustomerData.forEach(data => {
      this.viewforrejected.applicantName = data.appl1name
      this.viewforrejected.applicantId = data.applicationid
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale
      var rateOfInt = mclr * 1 + mclrmalefemale * 1
      this.viewforrejected.roi = rateOfInt.toFixed(2)
      this.viewforrejected.houselattitude = data.houselatitude;
      this.viewforrejected.houselongitude = data.houselongitude;
      this.viewforrejected.businesslattitude = data.businesslatitude;
      this.viewforrejected.businesslongitude = data.businesslongitude
    })
    this.viewforrejected.imagesrc = this.viewforrejected.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/getimage?solid=' + this.viewforrejected.id + '&applicationid=' + this.viewforrejected.applicantId + '&pdfType=ApplicantPhoto'
  }
  onnavigateHome() {
    this.ViewForRejectedService.onnavigateHome(this.viewforrejected.houselattitude, this.viewforrejected.houselongitude)
  }
  onnavigateBusiness() {
    this.ViewForRejectedService.onnavigateBusiness(this.viewforrejected.businesslattitude, this.viewforrejected.businesslongitude)
  }
  downloadPdf(event) {
    this.viewforrejected.documentValue = event.srcElement.attributes.value.value
    this.ViewForRejectedService.downloadPdf(this.viewforrejected.documentValue, this.viewforrejected.applicationid)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Pdf Does not exist")
        } else {
          var headers = item;
          saveAs(item, this.viewforrejected.filename + ".pdf");
        }
      }));
  }
  downloadzip(event) {
    this.viewforrejected.documentValue = event.srcElement.attributes.value.value
    this.viewforrejected.filename = this.viewforrejected.documentValue + "_" + this.viewforrejected.applicantName + "_" + this.viewforrejected.applicantId
    this.ViewForRejectedService.downloadzip(this.viewforrejected.documentValue, this.viewforrejected.filename)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Zip file is not Available")
        } else {
          var headers = item;
          saveAs(item, this.viewforrejected.filename + ".zip");
        }
      }));
  }
  downloadCibil(event) {
    // this.documentValue = event.srcElement.attributes.value.value
    // this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicantId
    // const base_url = this.common_IP+'/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatecibil?solid='+this.id+'&applicationid=' + this.applicantId + '&pdfType=' + this.documentValue
    // return this.http.post(
    //   base_url,
    //   {
    //     pdfType: this.documentValue,
    //   },
    //   { responseType: 'blob' }
    // ).subscribe((item => {
    //   if (item.size == 0) {
    //     alert("The Required Pdf Does not exist")
    //   } else {
    //     var headers = item;
    //     saveAs(item, this.filename + ".pdf");
    //   }
    // }));
    alert('Application under Construction');
  }
  navigatetoviewImage() {
    this.viewApplication.getImages().subscribe((data => {
      console.log(data)
      if (data['statusCode'] === 200) {
        this.router.navigate(['viewImage']);
      } else {
        alert('Images not found')
      }
    }))
  }
}