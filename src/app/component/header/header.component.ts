import { Component, OnInit } from '@angular/core';
import { HeaderModel } from 'src/app/model/header-model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  headermodel:HeaderModel

  constructor() { }


  ngOnInit() {
    this.headermodel = new HeaderModel();
    this.headermodel.id = sessionStorage.getItem('username')
    this.headermodel.branch = sessionStorage.getItem('branch')
  }
}