import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewForPendingModel } from 'src/app/model/view-for-pending-model';
import { Viewforpendingdisbursmentmodel } from 'src/app/model/viewforpendingdisbursmentmodel';
import { PendingfordisbursementService } from 'src/app/service/pendingfordisbursement.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { ViewForApprovedService } from 'src/app/service/view-for-approved.service';

@Component({
  selector: 'app-view-for-pending-disbursement',
  templateUrl: './view-for-pending-disbursement.component.html',
  styleUrls: ['./view-for-pending-disbursement.component.css']
})
export class ViewForPendingDisbursementComponent implements OnInit {

  viewforpendingdisbursmentmodel: Viewforpendingdisbursmentmodel;
  constructor(private http: HttpClient,
    private viewApplication: ViewApplicationService,
    private router: Router, private pendingfordisbursementservice: PendingfordisbursementService) { }
  pendingDisbursement: any = [];


  ngOnInit() {
    this.viewforpendingdisbursmentmodel = new Viewforpendingdisbursmentmodel();
    this.viewforpendingdisbursmentmodel.id = sessionStorage.getItem('username');
    this.viewforpendingdisbursmentmodel.common_IP = sessionStorage.getItem('commonIP');

    this.pendingDisbursement = JSON.parse(sessionStorage.getItem('pendingDisbursement'))
    this.pendingDisbursement.forEach(data => {
      console.log(data)
      this.viewforpendingdisbursmentmodel.applicantName = data.applicantName
      this.viewforpendingdisbursmentmodel.applicantId = data.applicationId;
      this.viewforpendingdisbursmentmodel.customerId = data.customerId;
      this.viewforpendingdisbursmentmodel.sanctionedLoanAmount = data.sanctionedLoanAmount
      this.viewforpendingdisbursmentmodel.savingAccountNumber = data.savingAccountNumber
      this.viewforpendingdisbursmentmodel.loanaccountNumber = data.loanaccountNumber;
      this.viewforpendingdisbursmentmodel.approvedDatetime = data.approvedDatetime;

      this.viewforpendingdisbursmentmodel.savingOpendDate = data.savingOpendDate
      this.viewforpendingdisbursmentmodel.loanAccountOpenDate = data.loanAccountOpenDate;
      this.viewforpendingdisbursmentmodel.disbursmentStatus = data.disbursmentStatus;

      this.viewforpendingdisbursmentmodel.disbursmentAmount = data.disbursmentAmount
      this.viewforpendingdisbursmentmodel.disbursmentDatetime = data.disbursmentDatetime;
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale
      var rateOfInt = mclr * 1 + mclrmalefemale * 1
      this.viewforpendingdisbursmentmodel.roi = rateOfInt.toFixed(2)
      this.viewforpendingdisbursmentmodel.houselattitude = data.houselatitude;
      this.viewforpendingdisbursmentmodel.houselongitude = data.houselongitude;
      this.viewforpendingdisbursmentmodel.businesslattitude = data.businesslatitude;
      this.viewforpendingdisbursmentmodel.businesslongitude = data.businesslongitude
    });
  }
  PostAccountData() {

    if (this.viewforpendingdisbursmentmodel.amount > this.viewforpendingdisbursmentmodel.sanctionedLoanAmount) {
      alert('Amount should be less then Disbursed Amount')
      window.location.reload()
     
    }
    this.pendingfordisbursementservice.postAccountData(this.viewforpendingdisbursmentmodel.DebitAccount, this.viewforpendingdisbursmentmodel.CreditAccount, this.viewforpendingdisbursmentmodel.amount, this.viewforpendingdisbursmentmodel.loanPeriod)
      .subscribe(data => {
        alert("Application Disbursed Successfully")
        this.router.navigate(['/pendingfordisbursement']).then(() => {
          window.location.reload();
        });
      })


  } PostAccountDatas() {

    this.http.post('http://172.18.1.7:8985/approval-service/approval/disburse', {
      solId: 8000,
      loanPeriod: this.viewforpendingdisbursmentmodel.loanPeriod,
      amount: this.viewforpendingdisbursmentmodel.amount,
      CreditAccount: this.viewforpendingdisbursmentmodel.CreditAccount,
      DebitAccount: this.viewforpendingdisbursmentmodel.DebitAccount
    })

      .subscribe(data => {
        console.log(data)
      });
  }
}