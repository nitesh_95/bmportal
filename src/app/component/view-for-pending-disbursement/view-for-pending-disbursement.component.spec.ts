import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForPendingDisbursementComponent } from './view-for-pending-disbursement.component';

describe('ViewForPendingDisbursementComponent', () => {
  let component: ViewForPendingDisbursementComponent;
  let fixture: ComponentFixture<ViewForPendingDisbursementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewForPendingDisbursementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForPendingDisbursementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
