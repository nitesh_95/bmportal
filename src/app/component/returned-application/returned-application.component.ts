import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReturnedModel } from 'src/app/model/returned-model';
import { ReturnedApplicationService } from 'src/app/service/returned-application.service';

@Component({
  selector: 'app-returned-application',
  templateUrl: './returned-application.component.html',
  styleUrls: ['./returned-application.component.css']
})
export class ReturnedApplicationComponent implements OnInit {

  returnedApplicationsmodel : ReturnedModel
  returnedApplications: any = []
  public pageSize: number = 5;
  searchText;
  id:any = sessionStorage.getItem('username')
  // p: number = 1;
  // returnCustomerData = [];
  // approvedForLoans: number;
  // common_IP :any;
  
  
  constructor(private returnedApplication:ReturnedApplicationService, private router :Router) { }
  ngOnInit(): void {
   this.getData();
   this.returnedApplicationsmodel = new ReturnedModel();
  }

  getData() {
    this.returnedApplication.getData(this.id)
    .subscribe((data) => {
      this.returnedApplications.push(data)
      this.returnedApplications = this.returnedApplications[0]
     
    })
    
  }
  fetchData(event) {
    this.returnedApplicationsmodel.returnCustomerData = []
    var selected_id = event.currentTarget.id
    this.returnedApplications.forEach(data => {
      if (selected_id == data.applicationid) {
        this.returnedApplicationsmodel.returnCustomerData.push(data)
        this.returnedApplication.returnCustomerDatas(this.returnedApplicationsmodel.returnCustomerData)
        this.router.navigateByUrl('/returned_applications')
      }
    })

  }
}