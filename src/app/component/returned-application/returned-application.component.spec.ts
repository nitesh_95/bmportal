import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnedApplicationComponent } from './returned-application.component';

describe('ReturnedApplicationComponent', () => {
  let component: ReturnedApplicationComponent;
  let fixture: ComponentFixture<ReturnedApplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnedApplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnedApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
