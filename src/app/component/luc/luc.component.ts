import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Luc } from 'src/app/model/luc';
import { saveAs } from "file-saver";
import { LucService } from 'src/app/service/luc.service';

@Component({
  selector: 'app-luc',
  templateUrl: './luc.component.html',
  styleUrls: ['./luc.component.css']
})
export class LucComponent implements OnInit {

  lucModel: Luc;
  lucApplications: any = []
  lucApplicationsCount: any;


  constructor(private http: HttpClient, private lucservice: LucService) { }

  ngOnInit(): void {
    this.lucModel = new Luc();
    this.getData()
  }
  getdropdownvalue(event) {
    let targetValue = event.currentTarget.value;
    let splitValue = targetValue.split(': ')
    this.lucModel.paginationNumber = splitValue[0];
    this.lucModel.dropdownvalues = splitValue[1]

  }


  downloadPdf(event) {
    this.lucModel.applicationId = event.currentTarget.id
    this.lucModel.documentValue = event.srcElement.attributes.value.value
    this.lucModel.filename = this.lucModel.documentValue + "_" + this.lucModel.applicationId
    this.lucservice.downloadPdf(this.lucModel.applicationId, this.lucModel.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Pdf Does not exist")
        } else {
          var headers = item;
          saveAs(item, this.lucModel.filename + ".pdf");
        }
      }));
  }

  getData() {
    this.lucApplications = []
    this.lucservice.getData(this.lucModel.dropdownvalues)
      .subscribe((data) => {
        this.lucApplications.push(data)
        this.lucApplications = this.lucApplications[0]
      })
  }

  getDataCount() {
    this.lucApplicationsCount = []
    this.lucservice.getDataCount()
      .subscribe((data) => {
        console.log(data)
        this.lucApplicationsCount = data
      })
  }

}

