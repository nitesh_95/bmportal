import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
import { ViewForApprovedService } from 'src/app/service/view-for-approved.service';
import { ViewForApprovedModel } from 'src/app/model/view-for-approved-model';

@Component({
  selector: 'app-view-for-approved',
  templateUrl: './view-for-approved.component.html',
  styleUrls: ['./view-for-approved.component.css']
})
export class ViewForApprovedComponent implements OnInit {

  viewforapproved: ViewForApprovedModel;
  constructor(private http: HttpClient,
    private viewApplication: ViewApplicationService,
    private router: Router, private viewforapprovedservice: ViewForApprovedService) { }
  approveCustomerData: any = [];


  ngOnInit() {
    this.viewforapproved = new ViewForApprovedModel();
    this.viewforapproved.id = sessionStorage.getItem('username');
    this.viewforapproved.common_IP = sessionStorage.getItem('commonIP');

    this.approveCustomerData = JSON.parse(sessionStorage.getItem('approvedCustomerData'))
    this.approveCustomerData.forEach(data => {
      this.viewforapproved.applicantName = data.appl1name
      this.viewforapproved.applicantId = data.applicationid;
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale
      var rateOfInt = mclr * 1 + mclrmalefemale * 1
      this.viewforapproved.roi = rateOfInt.toFixed(2)
      this.viewforapproved.houselattitude = data.houselatitude;
      this.viewforapproved.houselongitude = data.houselongitude;
      this.viewforapproved.businesslattitude = data.businesslatitude;
      this.viewforapproved.businesslongitude = data.businesslongitude
    });
    sessionStorage.setItem('applicantId', this.viewforapproved.applicantId);
    this.viewApplication.setUserName(this.viewforapproved.id)
    this.viewforapproved.imagesrc = this.viewforapproved.common_IP + '/customer/getimage?solid=' + this.viewforapproved.id + '&applicationid=' + this.viewforapproved.applicantId + '&pdfType=ApplicantPhoto'
  }
  onnavigateHome() {
    this.viewforapprovedservice.onnavigateHome(this.viewforapproved.houselattitude, this.viewforapproved.houselongitude)
  }
  onnavigateBusiness() {
    this.viewforapprovedservice.onnavigateHome(this.viewforapproved.businesslattitude, this.viewforapproved.businesslongitude)
  }
  downloadzip(event) {
    this.viewforapproved.applicantId = event.currentTarget.id;
    this.viewforapproved.documentValue = event.srcElement.attributes.value.value;
    this.viewforapproved.filename = this.viewforapproved.documentValue + "_" + this.viewforapproved.applicantName + "_" + this.viewforapproved.applicantId;
    this.viewforapprovedservice.downloadzip(this.viewforapproved.applicantId, this.viewforapproved.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Zip file is not Available")
        } else {
          var headers = item;
          saveAs(item, this.viewforapproved.filename + ".zip");
        }
      }));

  }
  downloadCibil(event) {

    //   this.applicantId = event.currentTarget.id;
    //   this.documentValue = event.srcElement.attributes.value.value;
    //   if(this.documentValue === 'CibilReportUpload'){
    //   this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicantId;
    //   const base_url = this.common_IP+'/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatecibil?solid='+this.id+'&applicationid=' + this.applicantId + '&pdfType=' + this.documentValue
    //   return this.http.post(
    //     base_url,
    //     {
    //       pdfType: this.documentValue,
    //     },
    //     { responseType: 'blob' }
    //   ).subscribe((item => {
    //     if (item.size == 0) {
    //       alert("The Required Pdf Does not exist")
    //     } else {
    //       var headers = item;
    //       saveAs(item, this.filename + ".pdf");
    //     }
    //   }));
    // } 
    alert('Application under Construction');
  }
  downloadPdf(event) {
    this.viewforapproved.applicantId = event.currentTarget.id
    this.viewforapproved.documentValue = event.srcElement.attributes.value.value;
    this.viewforapprovedservice.downloadPdf(this.viewforapproved.applicantId, this.viewforapproved.documentValue,)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Pdf Does not exist")
        } else {
          var headers = item;
          saveAs(item, this.viewforapproved.filename + ".pdf");
        }
      }));

  }
  navigatetoviewImage() {
    this.viewApplication.getImages().subscribe((data => {
      console.log(data)
      if (data['statusCode'] === 200) {
        this.router.navigate(['viewImage']);
      } else {
        alert('Images not found')
      }
    }))
  }
}

