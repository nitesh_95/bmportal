import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForApprovedComponent } from './view-for-approved.component';

describe('ViewForApprovedComponent', () => {
  let component: ViewForApprovedComponent;
  let fixture: ComponentFixture<ViewForApprovedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewForApprovedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
