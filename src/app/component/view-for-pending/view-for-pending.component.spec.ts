import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForPendingComponent } from './view-for-pending.component';

describe('ViewForPendingComponent', () => {
  let component: ViewForPendingComponent;
  let fixture: ComponentFixture<ViewForPendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewForPendingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
