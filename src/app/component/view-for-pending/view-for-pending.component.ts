import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";
declare var $: any;
import { DatePipe } from '@angular/common';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { ViewForPendingModel } from 'src/app/model/view-for-pending-model';
import { ViewForPendingService } from 'src/app/service/view-for-pending.service';

@Component({
  selector: 'app-view-for-pending',
  templateUrl: './view-for-pending.component.html',
  styleUrls: ['./view-for-pending.component.css']
})
export class ViewForPendingComponent implements OnInit {

  public additionalFiles: any = [];
  verifyDetails:any[] = [];
  verify: Map<string, {}> = new Map();
  viewforpending: ViewForPendingModel;
  pendingCustomerData: any = [];
  constructor(
    private http: HttpClient,
    private router: Router,
    private datepipe: DatePipe,
    private viewApplication: ViewApplicationService, private viewforpendingservice: ViewForPendingService) { }
  ngOnInit() {
    this.viewforpending = new ViewForPendingModel();
    this.viewforpending.id = sessionStorage.getItem('username')
    console.log(this.viewforpending.id)
    this.viewforpending.common_IP = sessionStorage.getItem('commonIP');
    this.pendingCustomerData = JSON.parse(sessionStorage.getItem('pendingCustomerData'))
    this.pendingCustomerData.forEach(data => {
      console.log(data)
      this.viewforpending.applicantName = data.appl1name
      this.viewforpending.applicantId = data.applicationid
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale
      var rateOfInt = mclr * 1 + mclrmalefemale * 1
      this.viewforpending.roi = rateOfInt.toFixed(2)
      console.log(this.viewforpending.roi)
      this.viewforpending.houselattitude = data.houselatitude;
      this.viewforpending.houselongitude = data.houselongitude;
      this.viewforpending.businesslattitude = data.businesslatitude;
      this.viewforpending.businesslongitude = data.businesslongitude
    })

    this.viewforpending.imagesrc = this.viewforpending.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/getimage?solid=' + this.viewforpending.id + '&applicationid=' + this.viewforpending.applicantId + '&pdfType=ApplicantPhoto'
    this.downloadzipss()

  }
  onnavigateHome() {
    this.viewforpendingservice.onnavigateHome(this.viewforpending.houselattitude, this.viewforpending.houselongitude)
  }
  onnavigateBusiness() {
    this.viewforpendingservice.onnavigateHome(this.viewforpending.businesslattitude, this.viewforpending.businesslongitude)
  }
  // matchingAccount(event) {
  //   if ($('#savingAccount').val() == '' || $('#confirmsavingAccount').val() == '') {
  //     $('#message').html('').css('color', 'white');
  //   } else if ($('#savingAccount').val() == $('#confirmsavingAccount').val()) {
  //     $('#message').html('Matching').css('color', 'green');
  //   } else {
  //     $('#message').html('Not Matching').css('color', 'red');
  //   }
  // }
  // matchingBankcustId(event) {
  //   if ($('#bankCustId').val() == '' || $('#confirmbankCustId').val() == '') {
  //     $('#nextMessage').html('').css('color', 'white');
  //   } else if ($('#bankCustId').val() == $('#confirmbankCustId').val()) {
  //     $('#nextMessage').html('Matching').css('color', 'green');
  //   } else {
  //     $('#nextMessage').html('Not Matching').css('color', 'red');
  //   }
  // }
  calculator(event) {
    var principalAmt = $("#loanAmount").val();
    var uhRecommendedAmt = +$('#uhRecommendedAmt').val()
    var rate = this.viewforpending.roi;
    var term = $("#NoOfMonths").val();

    var p = parseFloat(principalAmt);
    var r = parseFloat(rate);
    var t = parseFloat(term);
    var R = (r / (12)) / 100;
    console.log(p,r,t,R);
    var e = (p * R * (Math.pow((1 + R), t)) / ((Math.pow((1 + R), t)) - 1));
    if (p > uhRecommendedAmt) {
      alert("BM recommended amount should be less than Sub-k recommended amount")
      return false;
    } else {
      var finalamount = Math.round(e);
      $("#emiAmt").val(Math.round(e));
    }
  }
  downloadPdf(event) {
    this.viewforpending.documentValue = event.srcElement.attributes.value.value;
    this.viewforpending.filename = this.viewforpending.documentValue + "_" + this.viewforpending.applicantName + "_" + this.viewforpending.applicantId;
    this.viewforpendingservice.downloadPdf(this.viewforpending.applicantId, this.viewforpending.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Pdf Does not exist")
        } else {
          var headers = item;
          saveAs(item, this.viewforpending.filename + ".pdf");
        }
      }));

  }
  verifyAccount(){
    this.verify.clear();
    const base_URL = 'http://202.65.144.147:8985/approval-service/approval/getadditionaldetails?savingaccno=1247';
    this.http.get(base_URL).subscribe(data => {
      console.log(data);
      if(data["status"] === 200){
        this.verifyDetails.push(data);
        this.verifyDetails = this.verifyDetails[0];
        console.log(this.verifyDetails);
        this.verify.set('details',{Accountnumber:data['accountNumber'],customerName:data['customerName'],cifNumber:data['cifNumber']});
        console.log(this.verify.get);
        // this.viewforpending.savingAccount = data['accountNumber'];
        // this.viewforpending.confirmsavingAccount = data['accountNumber'];
        // this.viewforpending.bankCustId = data['cifNumber'];
        // this.viewforpending.confirmbankCustId = data['cifNumber'];
      }
    })
  }
  verifyOk(){
  for(let item of this.verify) {
    $('#verifyModal').modal('toggle');
    this.viewforpending.savingAccount = item[1]['Accountnumber'];
        this.viewforpending.confirmsavingAccount = item[1]['Accountnumber'];
        this.viewforpending.bankCustId = item[1]['cifNumber'];
        this.viewforpending.confirmbankCustId = item[1]['cifNumber'];
  }
  }
  getclickedId(event) {
    this.viewforpending.bmLoanAmount = this.viewforpending.bmLoanAmount
    this.viewforpending.bmEmi = this.viewforpending.bmEmi
    this.viewforpending.bmNoOfMonths = this.viewforpending.bmNoOfMonths
    this.viewforpending.remarks = this.viewforpending.remarks
    this.viewforpending.clickedTargetValue = event.currentTarget.value
    this.viewforpending.clickedTargetId = event.currentTarget.id
    if (this.viewforpending.showMe == false) {
      this.viewforpending.savingAccount = ''
      this.viewforpending.bankCustomerId = ''
      this.viewforpending.confirmSavingAccount = ''
      this.viewforpending.confirmbankCustomerId = ''
    } else {
      this.viewforpending.confirmSavingAccount = this.viewforpending.confirmSavingAccount
      this.viewforpending.confirmbankCustomerId = this.viewforpending.confirmSavingAccount
      this.viewforpending.savingAccount =this.viewforpending.savingAccount
      this.viewforpending.bankCustomerId =this.viewforpending.bankCustomerId
    }
    var rangeofMonths = this.viewforpending.bmNoOfMonths >= 12 && this.viewforpending.bmNoOfMonths <= 60;
    var tentativeDateValue = this.viewforpending.tentativeDispDate >= 1 && this.viewforpending.tentativeDispDate <= 31;
    var tentativeMonthValue = this.viewforpending.tentativeDispMonth >= 1 && this.viewforpending.tentativeDispMonth <= 12;
    var minLoanAmount = 51000
    var message = $('#message').html()
    var nextmessage = $('#nextMessage').html()
    if (this.viewforpending.clickedTargetId == 'Approved') {
      if (this.viewforpending.bmLoanAmount == '') {
        alert("Please Enter BM Recommendations Loan Amount")
      } else if (this.viewforpending.bmNoOfMonths == '') {
        alert("Please Enter BM Recommendations No.of Months")
      } else if (this.viewforpending.bmLoanAmount < minLoanAmount) {
        alert('Minimum BM Recommendations Loan Amount is 51,000')
      } else if (rangeofMonths == false) {
        alert("Number of months should be in between 12 and 60")
      } else if (this.viewforpending.tentativeDispDate != undefined && tentativeDateValue == false) {
        alert("Tentative Date should be in between 01 and 31");
      } else if (this.viewforpending.tentativeDispMonth != undefined && tentativeMonthValue == false) {
        alert("Tentative Month should be in between 01 and 12")
      } else if (this.viewforpending.showMe == true) {
        if (this.viewforpending.savingAccount === '') {
          alert("Please Enter Saving Account Number")
        } else if (this.viewforpending.confirmSavingAccount === '') {
          alert("Please Enter Confirm Saving Account Number")
        } else if (this.viewforpending.bankCustomerId === '') {
          alert("Please Enter Bank Customer ID")
        } else if (this.viewforpending.confirmbankCustomerId === '') {
          alert("Please Enter Confirm Bank Customer ID")
        } else if (this.viewforpending.savingAccount.length !== 13) {
          alert("Savings account number length should be 13 digits")
        } else if (this.viewforpending.confirmSavingAccount.length !== 13) {
          alert("Savings account number length should be 13 digits")
        } else if (this.viewforpending.bankCustomerId.length !== 8) {
          alert("Bank customer ID length should be 8 digits")
        } else if (this.viewforpending.confirmbankCustomerId.length !== 8) {
          alert("Bank customer ID length should be 8 digits")
        } else if (this.viewforpending.remarks == '') {
          alert("Please Enter Remarks")
        } else {
          $("#modalPopup").modal('show');
          this.viewforpending.clickedTargetId = event.currentTarget.id
          if (this.viewforpending.clickedTargetValue = 'Approve') {
            this.viewforpending.clickedTargetValue = event.currentTarget.value
          } else if (this.viewforpending.clickedTargetValue = 'Reject') {
            this.viewforpending.clickedTargetValue = event.currentTarget.value
          }
          else {
            this.viewforpending.clickedTargetValue = event.currentTarget.value
          }
        }
      } else if (this.viewforpending.remarks == '') {
        alert("Please Enter Remarks")
      } else {

        $("#modalPopup").modal('show');

        this.viewforpending.clickedTargetId = event.currentTarget.id
        this.viewforpending.clickedTargetValue = event.currentTarget.value
      }
    } else if (this.viewforpending.clickedTargetValue = 'Reject') {
      if (this.viewforpending.remarks == '') {
        alert("Please Enter Remarks")
      }
      else {
        $("#modalPopup").modal('show');
        this.viewforpending.clickedTargetId = event.currentTarget.id
        this.viewforpending.clickedTargetValue = event.currentTarget.value
      }

    } else {
      if (this.viewforpending.remarks == '') {
        alert("Please Enter Remarks")
      }
      else {
        $("#modalPopup").modal('show');
        this.viewforpending.clickedTargetId = event.currentTarget.id
        this.viewforpending.clickedTargetValue = event.currentTarget.value
      }

    }
  }
  downloadzip(event) {
    this.viewforpending.documentValue = event.srcElement.attributes.value.value
    this.viewforpending.filename = this.viewforpending.documentValue + "_" + this.viewforpending.applicantName + "_" + this.viewforpending.applicantId
    this.viewforpendingservice.downloadPdf(this.viewforpending.applicantId, this.viewforpending.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Zip file is not Available")
        } else {
          var headers = item;
          saveAs(item, this.viewforpending.filename + ".zip");
        }
      }));
  }

  downloadzipss() {
    this.viewforpendingservice.downloadzips(this.viewforpending.id, this.viewforpending.applicantId)
      .subscribe((item => {
        console.log(item)
        this.viewforpending.additionalFiles.push(item)
        this.viewforpending.additionalFiles = this.viewforpending.additionalFiles[0]
        console.log(this.viewforpending.additionalFiles)
      }));
  }

  downloadCibil(event) {
    // this.documentValue = event.srcElement.attributes.value.value
    // this.filename = this.documentValue + "_" + this.applicantName + "_" + this.applicantId
    // const base_url = this.common_IP+'/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatecibil?solid='+this.id+'&applicationid=' + this.applicantId + '&pdfType=' + this.documentValue
    // return this.http.post(
    //   base_url,
    //   {
    //     pdfType: this.documentValue,
    //   },
    //   { responseType: 'blob' }
    // ).subscribe((item => {
    //   if (item.size == 0) {
    //     alert("The Required Pdf Does not exist")
    //   } else {
    //     var headers = item;
    //     saveAs(item, this.filename + ".pdf");
    //   }
    // }));
    alert('Application under Construction');
  }

  getEndDate() {
    let maxdate = this.datepipe.transform(this.viewforpending.maxDate, 'yyyy-MM-dd')
    return maxdate;
  }
  getTodayDate() {

    return new Date().toISOString().split('T')[0];
  }
  getAllData(event) {
    if (this.viewforpending.tentativeDispDate == undefined || this.viewforpending.tentativeDispMonth == undefined || this.viewforpending.tentativeDispYear == undefined) {
      this.viewforpending.TentativeDisbursementDate = '';
    } else {
      this.viewforpending.TentativeDisbursementDate = this.viewforpending.tentativeDispDate + '-' + this.viewforpending.tentativeDispMonth + '-' + this.viewforpending.tentativeDispYear;
    }
    var targetId = event.currentTarget.id
    const base_url = this.viewforpending.common_IP + '/UBI_BM_APIGATEWAY-0.0.1-SNAPSHOT/approval-service/users/submit';
    // const base_url = this.common_IP+'/approval-service/users/submit';
    this.viewforpending.pendingCustomerData.forEach(data => {
      this.viewforpending.applicantId = data.applicationid
      this.viewforpending.applicantName = data.appl1name
      this.viewforpending.dateOfBirth = data.dobdate
      this.viewforpending.mobile = data.mobileno
      this.viewforpending.address = data.appl1address
      this.viewforpending.loanAmount = data.uhRecommendedLoanamount
      this.viewforpending.noOfMonths = data.uhRecommendedNoofmonths
      this.viewforpending.emi = data.uhRecommendedInstalmentamount
      this.viewforpending.rateofInterest = data.mclr * 1 + data.mclrmalefemale * 1,
        this.viewforpending.rate = this.viewforpending.rateofInterest.toFixed(2)
      this.viewforpending.bmLoanAmount = this.viewforpending.loanAmount
      this.viewforpending.bmEmi = this.viewforpending.bmEmi
      this.viewforpending.bmNoOfMonths = this.viewforpending.bmNoOfMonths
      this.viewforpending.remarks = this.viewforpending.remarks
      if (this.viewforpending.showMe == false) {
        this.viewforpending.confirmSavingAccount = '';
        this.viewforpending.confirmbankCustomerId = '';
      }
      else {
        this.viewforpending.confirmSavingAccount = this.viewforpending.confirmSavingAccount;
        this.viewforpending.confirmbankCustomerId = this.viewforpending.confirmbankCustomerId;
      }
      this.http.post(base_url, {
        solid: this.viewforpending.id,
        applicationid: this.viewforpending.applicantId,
        bankcustid: this.viewforpending.confirmbankCustomerId,
        bankremarks: this.viewforpending.remarks,
        loanamountsactioned: this.viewforpending.bmLoanAmount,
        instalmentamount: this.viewforpending.bmEmi,
        savingaccnumber: this.viewforpending.confirmSavingAccount,
        noofinstalments: this.viewforpending.bmNoOfMonths,
        bankverificationstatus: targetId,
        interestrate: this.viewforpending.rate,
        tentativeDispDate: this.viewforpending.TentativeDisbursementDate

      }).subscribe(data => {
        console.log(data)
        if (data['status'] === 200) {
          if (targetId == 'Approved') {
            alert("Application Approved Successfully")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else if (targetId == 'Returned') {
            alert("Application Returned Successfully")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else {
            alert("Application Rejected Successfully")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          }
        } else {
          if (targetId == 'Approved') {
            alert("Application Failed To Approved")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else if (targetId == 'Returned') {
            alert("Application Failed To Returned")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          } else {
            alert("Application Failed To Rejected")
            this.router.navigate(['/pending_for_review']).then(() => {
              window.location.reload();
            });
          }
        }

      })
    });
  }
  navigatetoviewImage() {
    this.viewApplication.getImages().subscribe((data => {
      console.log(data)
      if (data['statusCode'] === 200) {
        this.router.navigate(['viewImage']);
      } else {
        alert('Images not found')
      }
    }))
  }
}
