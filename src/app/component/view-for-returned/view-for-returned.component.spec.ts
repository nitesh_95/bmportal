import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewForReturnedComponent } from './view-for-returned.component';

describe('ViewForReturnedComponent', () => {
  let component: ViewForReturnedComponent;
  let fixture: ComponentFixture<ViewForReturnedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewForReturnedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewForReturnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
