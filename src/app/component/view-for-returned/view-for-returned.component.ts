import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from "file-saver";
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { Router } from '@angular/router';
import { ViewForReturnedModel } from 'src/app/model/view-for-returned-model';
import { ViewForReturnedService } from 'src/app/service/view-for-returned.service';

@Component({
  selector: 'app-view-for-returned',
  templateUrl: './view-for-returned.component.html',
  styleUrls: ['./view-for-returned.component.css']
})
export class ViewForReturnedComponent implements OnInit {
  viewforreturned: ViewForReturnedModel
  constructor(
    private http: HttpClient,
    private viewApplication: ViewApplicationService,
    private router: Router, private returnedService: ViewForReturnedService) { }
  returnCustomerData: any = [];

  ngOnInit() {
    this.viewforreturned = new ViewForReturnedModel();

    this.viewforreturned.id = sessionStorage.getItem('username')
    this.returnCustomerData = JSON.parse(sessionStorage.getItem('returnCustomerData'))
    this.viewforreturned.common_IP = sessionStorage.getItem('commonIP')
    this.returnCustomerData.forEach(data => {
      this.viewforreturned.applicantName = data.appl1name
      this.viewforreturned.applicantId = data.applicationid
      var mclr = +data.mclr
      var mclrmalefemale = +data.mclrmalefemale
      var rateOfInt = mclr * 1 + mclrmalefemale * 1
      this.viewforreturned.roi = rateOfInt.toFixed(2)
      this.viewforreturned.houselattitude = data.houselatitude;
      this.viewforreturned.houselongitude = data.houselongitude;
      this.viewforreturned.businesslattitude = data.businesslatitude;
      this.viewforreturned.businesslongitude = data.businesslongitude
    })
    this.viewforreturned.imagesrc = this.viewforreturned.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/getimage?solid=' + this.viewforreturned.id + '&applicationid=' + this.viewforreturned.applicantId + '&pdfType=ApplicantPhoto'
  }
  onnavigateHome() {
    this.returnedService.onnavigateHome(this.viewforreturned.houselattitude, this.viewforreturned.houselongitude)
  }
  onnavigateBusiness() {
    this.returnedService.onnavigateHome(this.viewforreturned.businesslattitude, this.viewforreturned.businesslongitude)
  }
  downloadPdf(event) {
    this.viewforreturned.applicantId = event.currentTarget.id
    this.viewforreturned.documentValue = event.srcElement.attributes.value.value;
    this.viewforreturned.filename = this.viewforreturned.documentValue + "_" + this.viewforreturned.applicantName + "_" + this.viewforreturned.applicantId
    this.returnedService.downloadPdf(this.viewforreturned.applicantId, this.viewforreturned.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Pdf Does not exist")
        } else {
          var headers = item;
          saveAs(item, this.viewforreturned.filename + ".pdf");
        }
      }));
  }
  downloadzip(event) {
    this.viewforreturned.applicantId = event.currentTarget.id
    this.viewforreturned.documentValue = event.srcElement.attributes.value.value
    this.viewforreturned.filename = this.viewforreturned.documentValue + "_" + this.viewforreturned.applicantName + "_" + this.viewforreturned.applicantId
    this.returnedService.downloadzip(this.viewforreturned.applicantId, this.viewforreturned.documentValue)
      .subscribe((item => {
        if (item.size == 0) {
          alert("The Required Zip file is not Available")
        } else {
          var headers = item;
          saveAs(item, this.viewforreturned.filename + ".zip");
        }
      }));
  }
  downloadCibil(event) {
    // this.applicantId = event.currentTarget.id
    // this.documentValue = event.srcElement.attributes.value.value
    // this.filename = this.documentValue+"_" +this.applicantName +"_"+this.applicantId
    // // const base_url = this.common_IP+'/documments-Service/customer/getpdf?solid='+this.id+'&applicationid='+this.applicationid+'&pdfType='+this.documentValue
    // const base_url = this.common_IP+'/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatecibil?solid='+this.id+'&applicationid=' + this.applicantId + '&pdfType=' + this.documentValue
    // return this.http.post(
    //   base_url,
    //   {
    //     pdfType: this.documentValue,
    //   },
    //   { responseType: 'blob' }
    // ).subscribe((item => {
    //   if (item.size == 0) {
    //     alert("The Required Pdf Does not exist")
    //   } else {
    //     var headers = item;
    //     saveAs(item, this.filename + ".pdf");
    //   }
    // }));
    alert('Application under Construction');
  }
  navigatetoviewImage() {
    this.viewApplication.getImages().subscribe((data => {
      console.log(data)
      if (data['statusCode'] === 200) {
        this.router.navigate(['viewImage']);
      } else {
        alert('Images not found')
      }
    }))
  }
}