import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PendingfordisbursementService {

  private pendingDisbursement: any = []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP + `/approval-service/approval/getCompletedDisbursment?solId=1247`;
  constructor(private http: HttpClient) { }


  public getData() {
    return this.http.get(`${this.base_url}`);
  }

  pendingDisbursements(pendingDisbursement: any) {
    this.pendingDisbursement = pendingDisbursement;

    sessionStorage.setItem('pendingDisbursement', JSON.stringify(this.pendingDisbursement))

  }

  postAccountData(DebitAccount: number, CreditAccount: any,amount:number,loanPeriod:number) {
   
    return this.http.post(this.common_IP + '/approval-service/approval/disburse', {
      solId: sessionStorage.getItem('username'),
      loanPeriod: loanPeriod,
      amount: amount,
      CreditAccount:CreditAccount,
      DebitAccount:DebitAccount
    })
  }
}