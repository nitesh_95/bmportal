import { TestBed } from '@angular/core/testing';

import { ViewForApprovedService } from './view-for-approved.service';

describe('ViewForApprovedService', () => {
  let service: ViewForApprovedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewForApprovedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
