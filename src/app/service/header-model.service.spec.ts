import { TestBed } from '@angular/core/testing';

import { HeaderModelService } from './header-model.service';

describe('HeaderModelService', () => {
  let service: HeaderModelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeaderModelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
