import { TestBed } from '@angular/core/testing';

import { RejectedApplicationService } from './rejected-application.service';

describe('RejectedApplicationService', () => {
  let service: RejectedApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RejectedApplicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
