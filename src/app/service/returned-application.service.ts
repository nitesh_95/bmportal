import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReturnedApplicationService {
  private returnCustomerData :any= []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP + `/fetchdata-Service/getReturnedApplications`;
  constructor(private http: HttpClient) { }


  public getData(solid: Number){
    return this.http.post(`${this.base_url}`,{solid});
}

returnCustomerDatas(returnCustomerData:any) {
    this.returnCustomerData = returnCustomerData;
  
    sessionStorage.setItem('returnCustomerData',JSON.stringify(this.returnCustomerData))
    
  }
  
}
