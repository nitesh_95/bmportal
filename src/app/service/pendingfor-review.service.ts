import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PendingforReviewService{
  private pendingCustomerData :any= []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP + `/fetchdata-Service/getPendingForReviewData`;
  constructor(private http: HttpClient) { }


  public getData(solid: Number){
    return this.http.post(`${this.base_url}`,{solid});
}

  pendingViewdata(pendingCustomerData:any) {
    this.pendingCustomerData = pendingCustomerData;
  
    sessionStorage.setItem('pendingCustomerData',JSON.stringify(this.pendingCustomerData))
    
  }
  
}
