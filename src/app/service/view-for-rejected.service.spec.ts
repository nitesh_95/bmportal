import { TestBed } from '@angular/core/testing';

import { ViewForRejectedService } from './view-for-rejected.service';

describe('ViewForRejectedService', () => {
  let service: ViewForRejectedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewForRejectedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
