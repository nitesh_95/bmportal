import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ViewForReturnedService {
  constructor(private http: HttpClient, private router: Router) { }
  common_IP = sessionStorage.getItem('commonIP');
  solid: any = sessionStorage.getItem('username');

  downloadPdf(documentValue: String, applicantId: any) {
    return this.http.post(this.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatepdf', {
      solid: sessionStorage.getItem('username'),
      applicationid: applicantId,
      pdfType: documentValue
    },
      { responseType: 'blob' });

  }

  onnavigateBusiness(businesslattitude: number, businesslongitude: number) {
    const url = 'http://maps.google.com/?q=' + businesslattitude + ',' + businesslongitude;
    return this.http.post(url, window.open(url, '_blank'))
  }
  onnavigateHome(houselatitude: number, houselongitude: number) {
    const url = 'http://maps.google.com/?q=' + houselatitude + ',' + houselongitude
    return this.http.post(url, window.open(url, '_blank'))
  }
  downloadzip(documentValue: String, applicantId: any) {

    return this.http.post(this.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/getzip?solid=' + this.solid + '&applicationid=' + applicantId, {
      solid: sessionStorage.getItem('username'),
      applicationid: applicantId,
      pdfType: documentValue
    },
      { responseType: 'blob' });

  }
}