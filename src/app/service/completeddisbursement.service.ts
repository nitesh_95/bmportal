import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CompleteddisbursementService {
  private completedDisbursement: any = []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP + `/approval-service/approval/getCompletedDisbursment?solId=1247`;
  constructor(private http: HttpClient) { }


  public getData() {
    return this.http.get(`${this.base_url}`);
  }

  completedDisbursements(completedDisbursement: any) {
    this.completedDisbursement = completedDisbursement;

    sessionStorage.setItem('completedDisbursement', JSON.stringify(this.completedDisbursement))

  }

}
