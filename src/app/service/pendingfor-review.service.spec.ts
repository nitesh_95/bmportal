import { TestBed } from '@angular/core/testing';

import { PendingforReviewService } from './pendingfor-review.service';

describe('PendingforReviewService', () => {
  let service: PendingforReviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PendingforReviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
