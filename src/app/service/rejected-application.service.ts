import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RejectedApplicationService {
  private rejectCustomerData: any = []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP + `/fetchdata-Service/getRejectedApplications`;
  constructor(private http: HttpClient) { }


  getData(solid: Number) {
    return this.http.post(`${this.base_url}`, { solid });
  }

  rejectCustomerDatas(rejectCustomerData: any) {
    this.rejectCustomerData = rejectCustomerData;
    sessionStorage.setItem('rejectCustomerData', JSON.stringify(this.rejectCustomerData))

  }

}
