import { TestBed } from '@angular/core/testing';

import { ViewApplicationService } from './view-application.service';

describe('ViewApplicationService', () => {
  let service: ViewApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewApplicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
