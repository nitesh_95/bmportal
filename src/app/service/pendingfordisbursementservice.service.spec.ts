import { TestBed } from '@angular/core/testing';

import { PendingfordisbursementserviceService } from './pendingfordisbursementservice.service';

describe('PendingfordisbursementserviceService', () => {
  let service: PendingfordisbursementserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PendingfordisbursementserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
