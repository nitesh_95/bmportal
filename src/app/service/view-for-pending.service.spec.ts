import { TestBed } from '@angular/core/testing';

import { ViewForPendingService } from './view-for-pending.service';

describe('ViewForPendingService', () => {
  let service: ViewForPendingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewForPendingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
