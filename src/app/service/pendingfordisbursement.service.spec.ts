import { TestBed } from '@angular/core/testing';

import { PendingfordisbursementService } from './pendingfordisbursement.service';

describe('PendingfordisbursementService', () => {
  let service: PendingfordisbursementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PendingfordisbursementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
