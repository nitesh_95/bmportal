import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ViewApplicationService  implements OnInit {
  private pendingCustomerData :any= []
  private approveCustomerData :any= []
  private rejectCustomerData :any= []
  private returnCustomerData :any= []

  private pendingfordisbursementData:any=[]

  constructor(public http:HttpClient) { }
  private applicantid = new BehaviorSubject<any>({});
  id:any;
  userName:any;
  common_IP:any;
ngOnInit(){
  
  console.log('service'+this.id);
}

  pendingViewdata(pendingCustomerData:any) {
    this.pendingCustomerData = pendingCustomerData;
 
    sessionStorage.setItem('pendingCustomerData',JSON.stringify(this.pendingCustomerData))
    
  }
  approveViewdata(approveCustomerData:any) {
    this.approveCustomerData = approveCustomerData;
    sessionStorage.setItem('approveCustomerData',JSON.stringify(this.approveCustomerData))

  }
  rejectViewdata(rejectCustomerData:any) {
    this.rejectCustomerData = rejectCustomerData;
    sessionStorage.setItem('rejectCustomerData',JSON.stringify(this.rejectCustomerData))

  }
  returnViewdata(returnCustomerData:any) {
    this.returnCustomerData = returnCustomerData;
    sessionStorage.setItem('returnCustomerData',JSON.stringify(this.returnCustomerData))

  }
  pendingfordisbursementDatas(pendingfordisbursementData:any) {
    this.pendingfordisbursementData = pendingfordisbursementData;
    sessionStorage.setItem('pendingfordisbursementData',JSON.stringify(this.pendingfordisbursementData))

  }
  
  
  setapplicantId(id:any){
    this.applicantid.next(id);
  }
  getapplicantId(){
    return this.applicantid.asObservable()
  }
  setUserName(userName:any) {
    
    
    console.log(this.id);
  }
  getuserName(){
    return this.userName.asObservable()
  }
  getImages() {
    this.userName = sessionStorage.getItem('applicantId');
    this.id = sessionStorage.getItem('username');
    this.common_IP = sessionStorage.getItem('commonIP');
    console.log(this.common_IP,this.id, this.userName)
    const base_URL = this.common_IP+'/getApplicantImages?solId='+this.id+'&applicationId='+this.userName;
    return this.http.get(base_URL);
  }
}


