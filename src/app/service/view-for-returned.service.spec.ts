import { TestBed } from '@angular/core/testing';

import { ViewForReturnedService } from './view-for-returned.service';

describe('ViewForReturnedService', () => {
  let service: ViewForReturnedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewForReturnedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
