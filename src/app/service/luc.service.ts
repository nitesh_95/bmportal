import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LucService {

  constructor(private http: HttpClient) { }

  common_IP = sessionStorage.getItem('commonIP');

  
  downloadPdf(applicationId:any,documentValue:any) {
   
    return this.http.post(this.common_IP + '/PdfGenerationUBI-0.0.1-SNAPSHOT/customer/generatecibil?solid=1247&applicationid=' + applicationId + '&pdfType=' + documentValue + "_" + applicationId, {
      pdfType: documentValue + "_" + applicationId,
    }
     ,
      { responseType: 'blob' })

  }


  getData(dropdownvalues: string) {
    console.log(this.common_IP)
    return this.http.get(this.common_IP + '/fetchdata-Service/customer?pageSize=5' );
  }
  getDataCount() {

    return this.http.get(this.common_IP + '/fetchdata-Service/customer/count');
  }

}