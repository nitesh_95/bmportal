import { TestBed } from '@angular/core/testing';

import { LucService } from './luc.service';

describe('LucService', () => {
  let service: LucService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LucService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
