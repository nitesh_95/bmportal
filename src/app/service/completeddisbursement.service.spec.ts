import { TestBed } from '@angular/core/testing';

import { CompleteddisbursementService } from './completeddisbursement.service';

describe('CompleteddisbursementService', () => {
  let service: CompleteddisbursementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompleteddisbursementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
