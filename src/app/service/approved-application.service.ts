import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApprovedApplicationService {
  private approvedCustomerData :any= []


  common_IP = sessionStorage.getItem('commonIP');


  base_url = this.common_IP+`/fetchdata-Service/getApprovedApplications`;
  constructor(private http: HttpClient) { }


  public getData(solid: Number){
    return this.http.post(`${this.base_url}`,{solid});
}

  approvedCustomerDatas(approvedCustomerData:any) {
    this.approvedCustomerData = approvedCustomerData;
  
    sessionStorage.setItem('approvedCustomerData',JSON.stringify(this.approvedCustomerData))
    
  }
  
}
