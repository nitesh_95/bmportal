import { TestBed } from '@angular/core/testing';

import { ReturnedApplicationService } from './returned-application.service';

describe('ReturnedApplicationService', () => {
  let service: ReturnedApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReturnedApplicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
