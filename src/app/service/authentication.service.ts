import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }


  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    localStorage.setItem('loggedIn', 'true')
    console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('username');
    
  }
}
