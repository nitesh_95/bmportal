import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http:HttpClient) { }

  common_IP = sessionStorage.getItem('commonIP');
  
  id = sessionStorage.getItem('username');

  getChartCount(startDateAfterTransform: string, endDateAfterTransform: string): Observable<any> {

    return this.http.get(this.common_IP+'/dashboard-Service/dashboard?From_Date='+startDateAfterTransform+'&To_Date='+endDateAfterTransform+'&solId='+this.id);
  }


}
