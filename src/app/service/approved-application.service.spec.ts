import { TestBed } from '@angular/core/testing';

import { ApprovedApplicationService } from './approved-application.service';

describe('ApprovedApplicationService', () => {
  let service: ApprovedApplicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApprovedApplicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
