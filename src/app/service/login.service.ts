import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  
  constructor(private http: HttpClient) { }

  checklogin(username: string, password: string): Observable<any> {
    sessionStorage.setItem('username', username)
    if(username==''||password==''){
      alert("Enter all the Credentials")
    }
    return this.http.get('http://172.18.1.7:8985/login-Service/login?UserId=' + username + '&Password=' +password);
  }

}
