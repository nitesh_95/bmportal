import { TestBed } from '@angular/core/testing';

import { AuthPassService } from './auth-pass.service';

describe('AuthPassService', () => {
  let service: AuthPassService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthPassService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
