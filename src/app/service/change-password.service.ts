import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {
  common_ip = sessionStorage.getItem('commonIP');
  solid = sessionStorage.getItem('username')

  constructor(private http: HttpClient) { }

  checkPassword(oldPassword: string, newPassword: string, confirmPassword: string): Observable<any> {
    console.log(oldPassword + " " + newPassword + " " + confirmPassword)
    if (oldPassword == '' || newPassword == '' || confirmPassword == '') {
      alert("Please enter all the fields");
      window.location.reload();
    } else if (oldPassword == newPassword) {
      alert("Old and new password are same")
      window.location.reload();
    } else if (newPassword != confirmPassword) {
      alert("New and Confirm Password do not match")
      window.location.reload();
    } else {
      let oldPasswords = oldPassword;
      let confirmPasswords = confirmPassword
      return this.http.get(this.common_ip + '/login-Service/changepassword?Solid=' + this.solid + '&oldPassword=' + oldPasswords + '&newPassword=' + confirmPasswords)
    }

  }
}