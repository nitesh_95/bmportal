export class ViewForApprovedModel {
    common_IP: any;
    googlemaphousemap: any;
    googlemapbusinessmap: any;
    imagesrc: any;
    roi: any;
    aadharNumber: any;
    base_url: any
    id: any;
    applicantId: any;
    applicantName: any;
    solid: number;
    documentValue: any;
    filename: any;
    houselattitude: number;
    houselongitude: number;
    businesslattitude: number;
    businesslongitude: number;
    applicationid: any;
    
}
