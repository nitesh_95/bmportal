export class ViewForRejectedModel {
    applicationid: any;
    common_IP: any;
    googlemaphousemap: any;
    googlemapbusinessmap: any;
    imagesrc: any;
    roi: any;
    aadharNumber: any;
    applicantId: number;
    applicantName: any;
    solid: number;
    documentValue: any;
    filename: any;
    base_url: any;
    id: any;
    houselattitude: number;
    houselongitude: number;
    businesslattitude: number;
    businesslongitude: number;
}
