export class ViewForReturnedModel {
    common_IP: any;
    imagesrc: any;
    roi: any;
    aadharNumber: any;
    applicantId: any;
    applicantName: any;
    solid: number;
    documentValue: any;
    filename: any;
    base_url: any;
    id: any;
    houselattitude: number;
    houselongitude: number;
    businesslattitude: number;
    businesslongitude: number;
    googlemaphousemap: any;
    googlemapbusinessmap: any;
}
