export class Dashboard {
    
    BarChart: any;
    PieChart: any;
    data: Date = new Date();
    id: any;
    common_IP: any;
    approvedLoan: any;
    pendingForReview: any;
    rejectedLoan: any;
    returnedLoan: any;
    
    public model: any = { date: { year: 2018, month: 10, day: 9 } };
    public date: any;
    public newDate: any;
    date1: any;
    date2: any;
    chartCountList: any = [];
    approvedCount: number;
    pendingCount: number;
    returnedCount: number;
    rejectedCount: number;
    // fromDate = this.selDate1.day + '/' + this.selDate1.month + '/' + this.selDate1.year;
    // toDate = this.selDate2.day + '/' + this.selDate2.month + '/' + this.selDate2.year;
    emptyFromDate: any;
    emptyToDate: any;
    fromDateTimestamp: any = '';
    toDateTimestamp: any = '';
    barchart;
    piechart;
}
