import * as moment from 'moment';
export class ViewForPendingModel {
    id: any;
    applicantId: any;
    applicantName: any;
    solid: number;
    documentValue: any;
    filename: any;
    dateOfBirth: any
    mobile: any
    address: any
    loanAmount: any
    noOfMonths: any
    emi: any
    bmLoanAmount: any
    bmNoOfMonths: any
    bmEmi: any
    savingAccount: any
    confirmSavingAccount: any
    bankCustomerId: any
    confirmbankCustomerId: any
    remarks: any
    bankstatus: any
    applicationid: any
    rateofInterest: any
    showMe: any = false;
    clickedTargetId: any;
    clickedTargetValue: any;
    pendingCustomerData: any = [];
    imagesrc: string;
    applicantID: number;
    googlemaphousemap: any;
    googlemapbusinessmap: any;
    max: any;
    confirmsavingAccount:any;
    roi: any;
    aadharNumber: any;
    bankCustId:any;
    confirmbankCustId :any;
    common_IP: any;
    rate: any;
    base_url: any;
    houselattitude: number;
    houselongitude: number;
    businesslattitude: number;
    businesslongitude: number;
    tentativeDispDate:any;
    TentativeDisbursementDate;
    additionalFiles:any = [];
    maxDate: moment.Moment = moment().add(30, 'days');
    minDate: moment.Moment = moment().add(0, 'days');
    tentativeDispMonth;
    tentativeDispYear;
    verifyDetails: any = [];
}
