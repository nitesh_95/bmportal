import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { LoginModel } from './model/login-model';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { PendingforreviewComponent } from './component/pendingforreview/pendingforreview.component';
import { ApprovedApplicationComponent } from './component/approved-application/approved-application.component';
import { ReturnedApplicationComponent } from './component/returned-application/returned-application.component';
import { RejectedApplicationComponent } from './component/rejected-application/rejected-application.component';
import { SliderComponent } from './component/slider/slider.component';
import { LogoutComponent } from './component/logout/logout.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { ViewimageComponentComponent } from './component/viewimage-component/viewimage-component.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { LucComponent } from './component/luc/luc.component';
import { ViewForPendingComponent } from './component/view-for-pending/view-for-pending.component';
import { ViewForApprovedComponent } from './component/view-for-approved/view-for-approved.component';
import { ViewForRejectedComponent } from './component/view-for-rejected/view-for-rejected.component';
import { ViewForReturnedComponent } from './component/view-for-returned/view-for-returned.component';
import { DatePipe } from '@angular/common';
import { PendingfordisbursementComponent } from './component/pendingfordisbursement/pendingfordisbursement.component';
import { CompleteddisbursementComponent } from './component/completeddisbursement/completeddisbursement.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { ViewForPendingDisbursementComponent } from './component/view-for-pending-disbursement/view-for-pending-disbursement.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PendingforreviewComponent,
    ApprovedApplicationComponent,
    ReturnedApplicationComponent,
    RejectedApplicationComponent,
    SliderComponent,
    LogoutComponent,
    ChangePasswordComponent,
    ViewimageComponentComponent,
    HeaderComponent,
    FooterComponent,
    LucComponent,
    ViewForPendingComponent,
    ViewForApprovedComponent,
    ViewForRejectedComponent,
    ViewForReturnedComponent,
    PendingfordisbursementComponent,
    CompleteddisbursementComponent,
    ViewForPendingDisbursementComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxDaterangepickerMd.forRoot(),
  ],
  providers: [DatePipe,LoginModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
